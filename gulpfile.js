var gulp = require('gulp');
var babel = require('gulp-babel');
var documentation = require('gulp-documentation');

gulp.task('default', function () {
    gulp.src('./src/stick.js')
      .pipe(documentation({ format: 'html' }))
      .pipe(gulp.dest('documentation'));

    return gulp.src('src/stick.js')
        .pipe(babel())
        .pipe(gulp.dest('dist'));
});

gulp.task('mdd', function(){
  gulp.src('./src/stick.js')
    .pipe(documentation({ format: 'md' }))
    .pipe(gulp.dest('documentation'));
});
