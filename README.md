# StickJS [![Code Climate](https://codeclimate.com/github/wski/stickjs/badges/gpa.svg)](https://codeclimate.com/github/wski/stickjs) [![Travis CI](https://travis-ci.org/wski/stickjs.svg?branch=master)](https://travis-ci.org/wski/stickjs)

StickJS aims to assist in controlling things with joysticks. The main use case for StickJS is controlling airborne vehicles (quadcopters, wings and more). Many features such as stick nomalization,
steady cam mode, fixed speed, direction locking and more help you capture steady images or other kinds of data.

## Docs
Some pretty documentation is avalible here:
[Documentation Here](https://rawgit.com/wski/stickjs/master/documentation/index.html)
