require("babel/polyfill");

/** @module StickJS */
var Stick = {
  config: {
    min: 0,
    max: 255,
    inverted: true,
    stabalize_cmd: () => console.log('stabalization not set'),
  },
  momentum: ()=>{},
  /** @function
   * @name lockingMode
   * @description travel at a set speed in steady direction, a higher deadzone is recommended.
   * @callback returns modified movement info
   * @param {number} threshold the tollerance for when to lock and change direction lock.
   * @param {object} directions the current directions.
  */
  lockingMode: ()=>{}, // continue on last given number above x value.
  normalize: ()=>{},
  /** @function
   * @name steady
   * @description travel at a set speed in any direction, a higher deadzone is recommended.
   * @callback provides movement at a constant pace
   * @param {number} speed the speed at wich you'd like to travel.
   * @param {object} directions the current directions.
  */
  steady: (callback, speed, directions)=>{
    callback({
      up: (directions.up > 0) ? speed : 0,
      down: (directions.down > 0) ? speed : 0,
      left: (directions.left > 0) ? speed : 0,
      right: (directions.right > 0) ? speed : 0
    });
  },
  repeat: (interval, deadzone)=>{}, // repeats directions poll's every nth ms for anything over deadzone
  /** @function
   * @name tolerate
   * @description returns a promise which will only happen if the value is outside of the deadzone, positive/negative agnostic.
   * @param {number} deadzone deadzone you'd like to use.
   * @param {object} directions current directions of the joystick (up down left right).
   * @param {boolean} stabalize optional param to run stabalize command when joystick is released.
  */
  tolerate: (deadzone, directions, stabalize)=>{
    return new Promise((resolve, reject) => {
      let sanitizedDirections = {
        up: (directions.up > deadzone) ? directions.up : 0,
        down: (directions.down > deadzone) ? directions.down : 0,
        left: (directions.left > deadzone) ? directions.left : 0,
        right: (directions.right > deadzone) ? directions.right : 0,
      };
      resolve(sanitizedDirections);
      if(sanitizedDirections.up === 0 &&
        sanitizedDirections.down === 0 &&
        sanitizedDirections.left === 0 &&
        sanitizedDirections.right === 0){
          // if stick stabaliziation is enabled the stabalize function will run whenever the stick is
          if(stabalize){
            Stick.config.stabalize_cmd();
          }
      }
    });
  },
  /** @function
   * @name getDirectionPercents
   * @description triggers a callback returning an object containing up down left right values
   * @callback provided an object containing directions in 0 - 1 for value
   * @param {object} coords x and y value of the stick.
  */
  getDirectionPercents: (callback, coords)=>{
    let mid = (Stick.config.max + Stick.config.min) / 2;

    let directions = {
      up: (coords.y > mid) ? Math.abs((mid - coords.y)/(Stick.config.max - mid)) : 0,
      down: (coords.y < mid) ? Math.abs((mid - coords.y)/(Stick.config.max - mid)) : 0,
      left: (coords.x > mid) ? Math.abs((mid - coords.x)/(Stick.config.max - mid)) : 0,
      right: (coords.x < mid) ? Math.abs((mid - coords.x)/(Stick.config.max - mid)) : 0
    };

    if(Stick.config.inverted){
      directions = {
        up: directions.down,
        down: directions.up,
        left: directions.right,
        right: directions.left
      };
    }

    callback(directions);
  }
};
module.exports = Stick;
