var Stick = require('../dist/stick.js');

describe("Execute provided funtion only if value is outside of deadzone", function() {
  it("executes function with value outside of deadzone", function() {
    var passed = false;

    Stick.tolerate(0.10, {
      up: 0.05,
      down: 0,
      left: 0.6,
      right: 0,
    }).then(function(values){
      passed = true;
      expect(values).toBe({
        up: 0,
        down: 0,
        left: 0.6,
        right: 0
      });
    });
  });
});
