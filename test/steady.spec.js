var Stick = require('../dist/stick.js');
Stick.config.inverted = false;

describe("Take a bunch of stuff and if it is above zero then make it set to speed", function() {
  it("returns a consistant speed for anything other than zero values", function() {
    var passed = false;

    Stick.steady(function(speed){
      expect(speed).toEqual({
        up: 0.3,
        down: 0,
        left: 0,
        right: 0.3
      });
    }, 0.3, {
      up: 0.4,
      down: 0,
      left: 0,
      right: 0.1
    });
  });
});
