var Stick = require('../dist/stick.js');
Stick.config.inverted = false;

describe("Return a percentage equal to half up when given y of 75 and a min + max of 0 + 100", function() {
  it("executes function with min max and y defined", function() {
    var passed = false;

    Stick.getDirectionPercents(function(directions){
      expect(directions.up.toFixed(2)).toBeCloseTo(0.18);
    }, {
      x: 150,
      y: 150
    });
  });
});

describe("Return a percentage equal to half down when given y of 25 and a min + max of 0 + 100", function() {
  it("executes function with min max and y defined", function() {
    var passed = false;

    Stick.getDirectionPercents(function(directions){
      expect(directions.down.toFixed(2)).toBeCloseTo(0.61);
    }, {
      x: 50,
      y: 50
    });
  });
});

describe("Return a percentage equal to half left when given x of 75 and a min + max of 0 + 100", function() {
  it("executes function with min max and y defined", function() {
    var passed = false;

    Stick.getDirectionPercents(function(directions){
      expect(directions.left.toFixed(2)).toBeCloseTo(0.18);
    }, {
      x: 150,
      y: 150
    });
  });
});

describe("Return a percentage equal to half right when given x of 25 and a min + max of 0 + 100", function() {
  it("executes function with min max and y defined", function() {
    var passed = false;

    Stick.getDirectionPercents(function(directions){
      expect(directions.right.toFixed(2)).toBeCloseTo(0.61);
    }, {
      x: 50,
      y: 50
    });
  });
});
